#include "Rect.h"
#include "utilities.hpp"

Rect::Rect(Vector2i position, Vector2i size)
	: position(position)
	, size(size)
{

}

void Rect::Move(Vector2i offset)
{
	position += offset;
}

Vector2i Rect::GetPosition() const
{
	return position;
}

void Rect::SetPosition(Vector2i value)
{
	position = value;
}

Vector2i Rect::GetSize() const
{
	return size;
}

void Rect::SetSize(Vector2i value)
{
	size = value;
}

Rect Rect::Intersection(Rect& another)
{
	return Rect(
		Vector2i(
			Max(position.x, another.position.x),
			Max(position.y, another.position.y)),
		Vector2i(
			Min(another.position.x + another.size.x - position.x, position.x + size.x - another.position.x),
			Min(another.position.y + another.size.y - position.y, position.y + size.y - another.position.y)));
}

Rect Rect::Union(Rect& another)
{
	return Rect(
		Vector2i(
			Min(position.x, another.position.x),
			Min(position.y, another.position.y)),
		Vector2i(
			Max(position.x + size.x - another.position.x, another.position.x + another.size.x - position.x),
			Max(position.y + size.y - another.position.y, another.position.y + another.size.y - position.y)));
}

string Rect::ToString()
{
	return  "X:      " + to_string(position.x)
		+ "\nY:      " + to_string(position.y)
		+ "\nWidth:  " + to_string(size.x)
		+ "\nHeight: " + to_string(size.y);
}
