#pragma once

template<typename T>
struct Vector2
{
	T x;
	T y;

	Vector2(T x, T y)
		: x(x)
		, y(y)
	{

	}

	constexpr Vector2 operator+(const Vector2& rhs) const
	{
		return Vector2(x + rhs.x, y + rhs.y);
	}

	constexpr Vector2 operator-(const Vector2& rhs) const
	{
		return Vector2(x - rhs.x, y - rhs.y);
	}

	constexpr Vector2 operator+=(const Vector2& rhs) const
	{
		return operator+(rhs);
	}

	constexpr Vector2 operator-=(const Vector2& rhs) const
	{
		return operator-(rhs);
	}
};

typedef Vector2<float> Vector2f;
typedef Vector2<int> Vector2i;