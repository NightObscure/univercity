#pragma once

#include <iostream>
#include <string>
#include <vector>

typedef unsigned int uint;

using namespace std;

template<typename T>
T Max(T a, T b)
{
	if (a > b) return a;
	else return b;
}

template<typename T>
T Min(T a, T b)
{
	if (a < b) return a;
	else return b;
}

template<typename T>
T Input(string name)
{
	T result;
	cout << name << ": ";
	cin >> result;
	return result;
}

template<typename T>
void Print(T message)
{
	cout << message << endl;
}