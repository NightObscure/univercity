#include <iostream>
#include <cmath>
#include <string>

using namespace std;

double Input(string name)
{
	double result;
	cout << name << ": ";
	cin >> result;
	return result;
}

void Do()
{
	double x = Input("X");
	double y = Input("Y");

	if (x * y > 0) cout <<(tan(x) + x / cbrt(y)) << endl;
	else if (x * y < 0) cout <<(log10(abs(pow(x, 2) * y))) << endl;
	else cout <<(pow(x, 3) + pow(sin(y), 2)) << endl;
}
