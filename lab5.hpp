#include <iostream>
#include <cmath>
#include <string>
#include <vector>
#include <ctime>

#include <windows.h>

using namespace std;

template<typename T>
T Input(string name)
{
	T result;
	cout << name << ": ";
	cin >> result;
	return result;
}

struct Matrix
{
private:
	int* data;
	int width;
	int height;

public:
	Matrix(int width, int height) : width(width), height(height)
	{
		data = new int[width * height];
	}

	~Matrix()
	{
		delete[] data;
	}

	int GetValue(int x, int y) const
	{
		return data[x * width + y];
	}

	void SetValue(int x, int y, int value)
	{
		data[x * width + y] = value;
	}

	int GetWidth() const
	{
		return width;
	}

	int GetHeight() const
	{
		return height;
	}
};

string format(int val, int len = 2)
{
	val %= (int)pow(10, len);
	int rlen = to_string(abs(val)).length();
	return (val >= 0 ? ' ' : '-') + string(len - rlen, ' ') + to_string(abs(val));
}

void Do()
{
	srand((int)time(0));

	HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);

	int width = Input<int>("Width");
	int height = Input<int>("Height");

	Matrix matr = Matrix(width, height);

	int result = 1;

	for (int y = 0; y < height; y++)
	{
		for (int x = 0; x < width; x++)
		{
			int item = rand() % 200 - 100;

			if (x >= y)
			{
				SetConsoleTextAttribute(hConsole, 12);
				if (item % 3 == 0) result *= item;
			}
			else SetConsoleTextAttribute(hConsole, 15);

			matr.SetValue(x, y, item);
			cout << format(item) << " ";
		}
		cout << endl;
	}
	SetConsoleTextAttribute(hConsole, 15);
	cout << "output: " << result << endl;

	system("pause");
}
