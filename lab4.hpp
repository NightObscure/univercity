#include <iostream>
#include <cmath>
#include <string>
#include <vector>

using namespace std;

template<typename T>
T Input(string name)
{
	T result;
	cout << name << ": ";
	cin >> result;
	return result;
}

void Do()
{
	vector<int> arr = vector<int>();
	int size = Input<int>("Size");
	int pos = 0;
	int minIndex = 0;
	int minPos = 0;

	for (int i = 0; i < size; i++)
	{
		int item = rand() % 200 - 100;
		arr.push_back(item);
		cout << item << " ";
		if (item < arr[minIndex])
		{
			minIndex = i;
			minPos = pos;
		}
		pos += to_string(item).length() + 1;
	}
	cout << endl;
	cout << string(minPos, ' ') << string(to_string(arr[minIndex]).length(), '^') << endl;

	int sum = 0;
	for (int i = minIndex + 1; i < size; i++)
	{
		sum += abs(arr[i]);
	}
	cout << "output: " << sum << endl;

	system("pause");
}
