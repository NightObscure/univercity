#pragma once

#include "Vector2.h"
#include "utilities.hpp"

class Rect
{
private:
	Vector2i position;
	Vector2i size;

public:
	Rect(Vector2i position, Vector2i size);

	void Move(Vector2i offset);

	Vector2i GetPosition() const;
	void SetPosition(Vector2i value);

	Vector2i GetSize() const;
	void SetSize(Vector2i value);

	Rect Intersection(Rect& another);
	Rect Union(Rect& another);

	string ToString();
};

