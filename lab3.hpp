#include <iostream>
#include <cmath>
#include <string>

using namespace std;

double Input(string name)
{
	double result;
	cout << name << ": ";
	cin >> result;
	return result;
}

void Output(double d)
{
	cout << "output: " << d << endl;
	system("pause");
}

void Do()
{
	double x = Input("X");

	for (int n = 1; n <= 20; n++)
	{
		cout << "n: " << n << " => ";
		double divider = sin(2 * n + x);
		if (divider == 0) cout << "unavailable n value" << endl;
		else cout <<(n * pow(x, n - 1)) / divider << endl;
	}

	system("pause");
}
