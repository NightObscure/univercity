#pragma once

#include "Rect.h"

void NewLine(uint count = 1)
{
	for (uint i = 0; i < count; i++) cout << endl;
}

void Do()
{
	Print("Rectangle 1");
	Rect r1 = Rect(Vector2i(Input<int>("X"), Input<int>("Y")), Vector2i(Input<int>("Width"), Input<int>("Height")));
	NewLine();

	Print("Rectangle 2");
	Rect r2 = Rect(Vector2i(Input<int>("X"), Input<int>("Y")), Vector2i(Input<int>("Width"), Input<int>("Height")));
	NewLine();

	Print("Intersection");
	Print(r1.Intersection(r2).ToString());
	NewLine();

	Print("Union");
	Print(r1.Union(r2).ToString());
	NewLine();
}