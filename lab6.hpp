#include <iostream>
#include <cmath>
#include <string>
#include <vector>
#include <ctime>

#include <Windows.h>

using namespace std;

template<typename T>
T Input(string name)
{
	T result;
	cout << name << ": ";
	cin >> result;
	return result;
}

struct Train
{
private:
	int number;
	int destination;
	int outcomingTime;
	int seats;

public:
	Train(int number, int destination, int outcomingTime, int seats) : number(number), destination(destination), outcomingTime(outcomingTime), seats(seats)
	{

	}

	int GetNumber() const
	{
		return number;
	}

	int GetDestination() const
	{
		return destination;
	}

	int GetOutcomingTime() const
	{
		return outcomingTime;
	}

	int GetSeats() const
	{
		return seats;
	}
};

string format(int val, int len = 3)
{
	val %= (int)pow(10, len);
	int rlen = to_string(abs(val)).length();
	return (val >= 0 ? ' ' : '-') + string(len - rlen, ' ') + to_string(abs(val));
}

void DrawMetrics(int sNumber)
{
	cout << string(4, ' ');
	for (int i = 0; i < sNumber; i++)
	{
		cout << i << string(10 - to_string(i).length(), ' ');
	}
	cout << endl;
}

void DrawLine(int to, int seats, int sNumber, HANDLE hConsole, int targetStation = -1)
{
	bool goodLine = targetStation >= 0 && to >= targetStation;
	if (goodLine) SetConsoleTextAttribute(hConsole, 12);
	cout << format(seats);
	for (int i = 0; i < sNumber; i++)
	{
		SetConsoleTextAttribute(hConsole, 15);
		cout << '|';

		if (goodLine)
		{
			if (i < targetStation) SetConsoleTextAttribute(hConsole, 12);
			else SetConsoleTextAttribute(hConsole, 14);
		}
		if (i < to) cout << string(9, '=');
		else cout << string(9, ' ');
	}
	cout << endl;
	SetConsoleTextAttribute(hConsole, 15);
}

void DrawGraph(vector<Train> trains, int sNumber, HANDLE hConsole, int targetStation = -1)
{
	DrawMetrics(sNumber);
	for (auto train : trains) DrawLine(train.GetDestination(), train.GetSeats(), sNumber, hConsole, targetStation);
	DrawMetrics(sNumber);
}

void Do()
{
	srand((int)time(0));

	HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);

	int tNumber = Input<int>("Trains number");
	int sNumber = Input<int>("Stations number");
	vector<Train> trains = vector<Train>();

	int numCounter = rand() % 500;

	for (int i = 0; i < tNumber; i++)
	{
		Train train = Train(numCounter++, rand() % sNumber, abs(rand()), rand() % 40 * 4);
		trains.push_back(train);
	}

	DrawGraph(trains, sNumber, hConsole);

	int targetStation = Input<int>("Target station");

	SetConsoleCursorPosition(hConsole, { 0, 2 });
	DrawGraph(trains, sNumber, hConsole, targetStation);
	SetConsoleCursorPosition(hConsole, { 0, 5 + (SHORT)tNumber });

	int sum = 0;
	for (auto train : trains)
	{
		if (train.GetDestination() >= targetStation) sum += train.GetSeats();
	}

	cout << "Total seats: " << sum << endl;

	system("pause");
}
